/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include "stdio.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc3;

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;

DMA2D_HandleTypeDef hdma2d;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c3;

LTDC_HandleTypeDef hltdc;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim8;

UART_HandleTypeDef huart7;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart6;

SDRAM_HandleTypeDef hsdram1;

osThreadId defaultTaskHandle;
osThreadId myTask04Handle;
osThreadId myTask02Handle;
osThreadId myTask03Handle;
osThreadId myTask07Handle;
osThreadId myTask08Handle;
osThreadId myTask09Handle;
osMessageQId zqsdHandle;
osMessageQId Lance_el_svtHandle;
osMessageQId incr_scoreHandle;
osMessageQId endgameHandle;
osMessageQId joystickHandle;
osMutexId MutexEcranHandle;
osMutexId MutextableauHandle;
osMutexId positiontrucquibougeHandle;
osMutexId MutexelsvtHandle;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC3_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C3_Init(void);
static void MX_LTDC_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM8_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_DAC_Init(void);
static void MX_UART7_Init(void);
static void MX_FMC_Init(void);
static void MX_DMA2D_Init(void);
void StartDefaultTask(void const * argument);
void Affichage_temps(void const * argument);
void Pave(void const * argument);
void El_suivant(void const * argument);
void GameOver(void const * argument);
void Joystick(void const * argument);
void Effacelignes(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
	uint8_t rxbuffer[10];
	uint16_t pos[3];
	uint16_t positiontkb[7];
	uint16_t elsvt[2] = {1,1};
	uint8_t Black_Board[200];
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC3_Init();
  MX_I2C1_Init();
  MX_I2C3_Init();
  MX_LTDC_Init();
  MX_RTC_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM5_Init();
  MX_TIM8_Init();
  MX_USART1_UART_Init();
  MX_USART6_UART_Init();
  MX_ADC1_Init();
  MX_DAC_Init();
  MX_UART7_Init();
  MX_FMC_Init();
  MX_DMA2D_Init();
  /* USER CODE BEGIN 2 */
  BSP_LCD_Init();
  BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
  BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS+ BSP_LCD_GetXSize()*BSP_LCD_GetYSize()*4);
  BSP_LCD_DisplayOn();
  BSP_LCD_SelectLayer(1);
  BSP_LCD_Clear(LCD_COLOR_LIGHTGREEN);
  BSP_LCD_SetFont(&Font12);
  BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
  BSP_LCD_SetBackColor(LCD_COLOR_LIGHTGREEN);

  BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());

  MX_USART1_UART_Init();

  HAL_UART_Receive_IT(&huart1,rxbuffer,1);
  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of MutexEcran */
  osMutexDef(MutexEcran);
  MutexEcranHandle = osMutexCreate(osMutex(MutexEcran));

  /* definition and creation of Mutextableau */
  osMutexDef(Mutextableau);
  MutextableauHandle = osMutexCreate(osMutex(Mutextableau));

  /* definition and creation of positiontrucquibouge */
  osMutexDef(positiontrucquibouge);
  positiontrucquibougeHandle = osMutexCreate(osMutex(positiontrucquibouge));

  /* definition and creation of Mutexelsvt */
  osMutexDef(Mutexelsvt);
  MutexelsvtHandle = osMutexCreate(osMutex(Mutexelsvt));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of zqsd */
  osMessageQDef(zqsd, 16, uint16_t);
  zqsdHandle = osMessageCreate(osMessageQ(zqsd), NULL);

  /* definition and creation of Lance_el_svt */
  osMessageQDef(Lance_el_svt, 16, uint16_t);
  Lance_el_svtHandle = osMessageCreate(osMessageQ(Lance_el_svt), NULL);

  /* definition and creation of incr_score */
  osMessageQDef(incr_score, 16, uint16_t);
  incr_scoreHandle = osMessageCreate(osMessageQ(incr_score), NULL);

  /* definition and creation of endgame */
  osMessageQDef(endgame, 16, uint16_t);
  endgameHandle = osMessageCreate(osMessageQ(endgame), NULL);

  /* definition and creation of joystick */
  osMessageQDef(joystick, 16, uint16_t);
  joystickHandle = osMessageCreate(osMessageQ(joystick), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityIdle, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTask04 */
  osThreadDef(myTask04, Affichage_temps, osPriorityLow, 0, 1024);
  myTask04Handle = osThreadCreate(osThread(myTask04), NULL);

  /* definition and creation of myTask02 */
  osThreadDef(myTask02, Pave, osPriorityNormal, 0, 1500);
  myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of myTask03 */
  osThreadDef(myTask03, El_suivant, osPriorityLow, 0, 512);
  myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* definition and creation of myTask07 */
  osThreadDef(myTask07, GameOver, osPriorityHigh, 0, 512);
  myTask07Handle = osThreadCreate(osThread(myTask07), NULL);

  /* definition and creation of myTask08 */
  osThreadDef(myTask08, Joystick, osPriorityIdle, 0, 128);
  myTask08Handle = osThreadCreate(osThread(myTask08), NULL);

  /* definition and creation of myTask09 */
  osThreadDef(myTask09, Effacelignes, osPriorityBelowNormal, 0, 2048);
  myTask09Handle = osThreadCreate(osThread(myTask09), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC|RCC_PERIPHCLK_RTC
                              |RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART6
                              |RCC_PERIPHCLK_UART7|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_I2C3;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 384;
  PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;
  PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV8;
  PeriphClkInitStruct.PLLSAIDivQ = 1;
  PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_8;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Uart7ClockSelection = RCC_UART7CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC3_Init(void)
{

  /* USER CODE BEGIN ADC3_Init 0 */

  /* USER CODE END ADC3_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC3_Init 1 */

  /* USER CODE END ADC3_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc3.Instance = ADC3;
  hadc3.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc3.Init.Resolution = ADC_RESOLUTION_12B;
  hadc3.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc3.Init.ContinuousConvMode = DISABLE;
  hadc3.Init.DiscontinuousConvMode = DISABLE;
  hadc3.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc3.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc3.Init.NbrOfConversion = 1;
  hadc3.Init.DMAContinuousRequests = DISABLE;
  hadc3.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc3) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC3_Init 2 */

  /* USER CODE END ADC3_Init 2 */

}

/**
  * @brief DAC Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC_Init(void)
{

  /* USER CODE BEGIN DAC_Init 0 */

  /* USER CODE END DAC_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC_Init 1 */

  /* USER CODE END DAC_Init 1 */
  /** DAC Initialization
  */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT1 config
  */
  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC_Init 2 */

  /* USER CODE END DAC_Init 2 */

}

/**
  * @brief DMA2D Initialization Function
  * @param None
  * @retval None
  */
static void MX_DMA2D_Init(void)
{

  /* USER CODE BEGIN DMA2D_Init 0 */

  /* USER CODE END DMA2D_Init 0 */

  /* USER CODE BEGIN DMA2D_Init 1 */

  /* USER CODE END DMA2D_Init 1 */
  hdma2d.Instance = DMA2D;
  hdma2d.Init.Mode = DMA2D_M2M;
  hdma2d.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
  hdma2d.Init.OutputOffset = 0;
  hdma2d.LayerCfg[1].InputOffset = 0;
  hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
  hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d.LayerCfg[1].InputAlpha = 0;
  if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_DMA2D_ConfigLayer(&hdma2d, 1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DMA2D_Init 2 */

  /* USER CODE END DMA2D_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00C0EAFF;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C3_Init(void)
{

  /* USER CODE BEGIN I2C3_Init 0 */

  /* USER CODE END I2C3_Init 0 */

  /* USER CODE BEGIN I2C3_Init 1 */

  /* USER CODE END I2C3_Init 1 */
  hi2c3.Instance = I2C3;
  hi2c3.Init.Timing = 0x00C0EAFF;
  hi2c3.Init.OwnAddress1 = 0;
  hi2c3.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c3.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c3.Init.OwnAddress2 = 0;
  hi2c3.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c3.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c3.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c3) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c3, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c3, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C3_Init 2 */

  /* USER CODE END I2C3_Init 2 */

}

/**
  * @brief LTDC Initialization Function
  * @param None
  * @retval None
  */
static void MX_LTDC_Init(void)
{

  /* USER CODE BEGIN LTDC_Init 0 */

  /* USER CODE END LTDC_Init 0 */

  LTDC_LayerCfgTypeDef pLayerCfg = {0};

  /* USER CODE BEGIN LTDC_Init 1 */

  /* USER CODE END LTDC_Init 1 */
  hltdc.Instance = LTDC;
  hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
  hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
  hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
  hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
  hltdc.Init.HorizontalSync = 40;
  hltdc.Init.VerticalSync = 9;
  hltdc.Init.AccumulatedHBP = 53;
  hltdc.Init.AccumulatedVBP = 11;
  hltdc.Init.AccumulatedActiveW = 533;
  hltdc.Init.AccumulatedActiveH = 283;
  hltdc.Init.TotalWidth = 565;
  hltdc.Init.TotalHeigh = 285;
  hltdc.Init.Backcolor.Blue = 0;
  hltdc.Init.Backcolor.Green = 0;
  hltdc.Init.Backcolor.Red = 0;
  if (HAL_LTDC_Init(&hltdc) != HAL_OK)
  {
    Error_Handler();
  }
  pLayerCfg.WindowX0 = 0;
  pLayerCfg.WindowX1 = 480;
  pLayerCfg.WindowY0 = 0;
  pLayerCfg.WindowY1 = 272;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB565;
  pLayerCfg.Alpha = 255;
  pLayerCfg.Alpha0 = 0;
  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  pLayerCfg.FBStartAdress = 0xC0000000;
  pLayerCfg.ImageWidth = 480;
  pLayerCfg.ImageHeight = 272;
  pLayerCfg.Backcolor.Blue = 0;
  pLayerCfg.Backcolor.Green = 0;
  pLayerCfg.Backcolor.Red = 0;
  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN LTDC_Init 2 */

  /* USER CODE END LTDC_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;
  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A
  */
  sAlarm.AlarmTime.Hours = 0x0;
  sAlarm.AlarmTime.Minutes = 0x0;
  sAlarm.AlarmTime.Seconds = 0x0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm B
  */
  sAlarm.Alarm = RTC_ALARM_B;
  if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the TimeStamp
  */
  if (HAL_RTCEx_SetTimeStamp(&hrtc, RTC_TIMESTAMPEDGE_RISING, RTC_TIMESTAMPPIN_POS1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 4294967295;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_DISABLE;
  sSlaveConfig.InputTrigger = TIM_TS_ITR0;
  if (HAL_TIM_SlaveConfigSynchro(&htim3, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 0;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 4294967295;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief TIM8 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM8_Init(void)
{

  /* USER CODE BEGIN TIM8_Init 0 */

  /* USER CODE END TIM8_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM8_Init 1 */

  /* USER CODE END TIM8_Init 1 */
  htim8.Instance = TIM8;
  htim8.Init.Prescaler = 0;
  htim8.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim8.Init.Period = 65535;
  htim8.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim8.Init.RepetitionCounter = 0;
  htim8.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim8) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim8, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim8, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM8_Init 2 */

  /* USER CODE END TIM8_Init 2 */

}

/**
  * @brief UART7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART7_Init(void)
{

  /* USER CODE BEGIN UART7_Init 0 */

  /* USER CODE END UART7_Init 0 */

  /* USER CODE BEGIN UART7_Init 1 */

  /* USER CODE END UART7_Init 1 */
  huart7.Instance = UART7;
  huart7.Init.BaudRate = 115200;
  huart7.Init.WordLength = UART_WORDLENGTH_8B;
  huart7.Init.StopBits = UART_STOPBITS_1;
  huart7.Init.Parity = UART_PARITY_NONE;
  huart7.Init.Mode = UART_MODE_TX_RX;
  huart7.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart7.Init.OverSampling = UART_OVERSAMPLING_16;
  huart7.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart7.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart7) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART7_Init 2 */

  /* USER CODE END UART7_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */
  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  huart6.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart6.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/* FMC initialization function */
static void MX_FMC_Init(void)
{

  /* USER CODE BEGIN FMC_Init 0 */

  /* USER CODE END FMC_Init 0 */

  FMC_SDRAM_TimingTypeDef SdramTiming = {0};

  /* USER CODE BEGIN FMC_Init 1 */

  /* USER CODE END FMC_Init 1 */

  /** Perform the SDRAM1 memory initialization sequence
  */
  hsdram1.Instance = FMC_SDRAM_DEVICE;
  /* hsdram1.Init */
  hsdram1.Init.SDBank = FMC_SDRAM_BANK1;
  hsdram1.Init.ColumnBitsNumber = FMC_SDRAM_COLUMN_BITS_NUM_8;
  hsdram1.Init.RowBitsNumber = FMC_SDRAM_ROW_BITS_NUM_12;
  hsdram1.Init.MemoryDataWidth = FMC_SDRAM_MEM_BUS_WIDTH_16;
  hsdram1.Init.InternalBankNumber = FMC_SDRAM_INTERN_BANKS_NUM_4;
  hsdram1.Init.CASLatency = FMC_SDRAM_CAS_LATENCY_1;
  hsdram1.Init.WriteProtection = FMC_SDRAM_WRITE_PROTECTION_DISABLE;
  hsdram1.Init.SDClockPeriod = FMC_SDRAM_CLOCK_DISABLE;
  hsdram1.Init.ReadBurst = FMC_SDRAM_RBURST_DISABLE;
  hsdram1.Init.ReadPipeDelay = FMC_SDRAM_RPIPE_DELAY_0;
  /* SdramTiming */
  SdramTiming.LoadToActiveDelay = 16;
  SdramTiming.ExitSelfRefreshDelay = 16;
  SdramTiming.SelfRefreshTime = 16;
  SdramTiming.RowCycleDelay = 16;
  SdramTiming.WriteRecoveryTime = 16;
  SdramTiming.RPDelay = 16;
  SdramTiming.RCDDelay = 16;

  if (HAL_SDRAM_Init(&hsdram1, &SdramTiming) != HAL_OK)
  {
    Error_Handler( );
  }

  /* USER CODE BEGIN FMC_Init 2 */

  /* USER CODE END FMC_Init 2 */
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOJ_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOK_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED1_Pin|LED2B14_Pin|LED3B15_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, LED14_Pin|LED15_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED16_GPIO_Port, LED16_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOI, LED3_Pin|LED4_Pin|LED1I1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_BL_CTRL_GPIO_Port, LCD_BL_CTRL_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_DISP_GPIO_Port, LCD_DISP_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOH, LED13_Pin|LED17_Pin|LED11_Pin|LED12_Pin
                          |LED2_Pin|LED18_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(EXT_RST_GPIO_Port, EXT_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PE3 */
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : ULPI_D7_Pin ULPI_D6_Pin ULPI_D5_Pin ULPI_D2_Pin
                           ULPI_D1_Pin ULPI_D4_Pin */
  GPIO_InitStruct.Pin = ULPI_D7_Pin|ULPI_D6_Pin|ULPI_D5_Pin|ULPI_D2_Pin
                          |ULPI_D1_Pin|ULPI_D4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : LED1_Pin LED2B14_Pin LED3B15_Pin */
  GPIO_InitStruct.Pin = LED1_Pin|LED2B14_Pin|LED3B15_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : BP2_Pin BP1_Pin */
  GPIO_InitStruct.Pin = BP2_Pin|BP1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED14_Pin LED15_Pin */
  GPIO_InitStruct.Pin = LED14_Pin|LED15_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_VBUS_Pin */
  GPIO_InitStruct.Pin = OTG_FS_VBUS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OTG_FS_VBUS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : Audio_INT_Pin */
  GPIO_InitStruct.Pin = Audio_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Audio_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : OTG_FS_PowerSwitchOn_Pin LED16_Pin */
  GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin|LED16_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : LED3_Pin LED4_Pin LED1I1_Pin LCD_DISP_Pin */
  GPIO_InitStruct.Pin = LED3_Pin|LED4_Pin|LED1I1_Pin|LCD_DISP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : uSD_Detect_Pin */
  GPIO_InitStruct.Pin = uSD_Detect_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(uSD_Detect_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_BL_CTRL_Pin */
  GPIO_InitStruct.Pin = LCD_BL_CTRL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LCD_BL_CTRL_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
  GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : TP3_Pin NC2_Pin */
  GPIO_InitStruct.Pin = TP3_Pin|NC2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : LED13_Pin LED17_Pin LED11_Pin LED12_Pin
                           LED2_Pin LED18_Pin */
  GPIO_InitStruct.Pin = LED13_Pin|LED17_Pin|LED11_Pin|LED12_Pin
                          |LED2_Pin|LED18_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pin : PI0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_INT_Pin */
  GPIO_InitStruct.Pin = LCD_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(LCD_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : ULPI_NXT_Pin */
  GPIO_InitStruct.Pin = ULPI_NXT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(ULPI_NXT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : BP_3_Pin BP_1_Pin RMII_RXER_Pin */
  GPIO_InitStruct.Pin = BP_3_Pin|BP_1_Pin|RMII_RXER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : ULPI_STP_Pin ULPI_DIR_Pin */
  GPIO_InitStruct.Pin = ULPI_STP_Pin|ULPI_DIR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : EXT_RST_Pin */
  GPIO_InitStruct.Pin = EXT_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(EXT_RST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : ULPI_CLK_Pin ULPI_D0_Pin */
  GPIO_InitStruct.Pin = ULPI_CLK_Pin|ULPI_D0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void Affiche_Pavee(uint16_t X, uint16_t Y, uint16_t carre1, uint16_t carre2, uint16_t carre3, uint16_t carre4, uint16_t num_couleur)
{
  /* USER CODE BEGIN Affiche_Pave */
	uint32_t couleur = 0;
	char texte[30]={};
	uint16_t Message[6] = {X, Y, carre1, carre2, carre3, carre4};
	Point point[4];
	point[0].X = 0; point[0].Y = 0;
	point[1].X = 0; point[1].Y = 0;
	point[2].X = 0; point[2].Y = 0;
	point[3].X = 0; point[3].Y = 0;

	switch(num_couleur)
	{
		case 0:
			{
				couleur = LCD_COLOR_LIGHTGREEN;
				break;
			}
		case 1:
			{
				couleur = LCD_COLOR_BLUE;
				break;
			}
		case 2:
			{
				couleur = LCD_COLOR_MAGENTA;
				break;
			}
		case 3:
			{
				couleur = LCD_COLOR_DARKCYAN;
				break;
			}
		case 4:
			{
				couleur = LCD_COLOR_ORANGE;
				break;
			}
		case 5:
			{
				couleur = LCD_COLOR_RED;
				break;
			}
	}

	sprintf(texte, "%d %d %d %d %d %d %d ", X, Y, carre1, carre2, carre3, carre4, num_couleur);
	BSP_LCD_SetTextColor(LCD_COLOR_RED);
	BSP_LCD_DisplayStringAt(0, LINE(15), texte, RIGHT_MODE);

	for (int i=0 ; i<4 ; i++)
	{
		switch(Message[i+2])
		{
			case 0:
			{
				point[0].X = Message[0]-25; point[0].Y = Message[1]-25;
				point[1].X = Message[0]-14; point[1].Y = Message[1]-25;
				point[2].X = Message[0]-14; point[2].Y = Message[1]-14;
				point[3].X = Message[0]-25; point[3].Y = Message[1]-14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 1:
			{
				point[0].X = Message[0]-12; point[0].Y = Message[1]-25;
				point[1].X = Message[0]-1; point[1].Y = Message[1]-25;
				point[2].X = Message[0]-1; point[2].Y = Message[1]-14;
				point[3].X = Message[0]-12; point[3].Y = Message[1]-14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 2:
			{
				point[0].X = Message[0]+12; point[0].Y = Message[1]-25;
				point[1].X = Message[0]+1; point[1].Y = Message[1]-25;
				point[2].X = Message[0]+1; point[2].Y = Message[1]-14;
				point[3].X = Message[0]+12; point[3].Y = Message[1]-14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 3:
			{
				point[0].X = Message[0]+25; point[0].Y = Message[1]-25;
				point[1].X = Message[0]+14; point[1].Y = Message[1]-25;
				point[2].X = Message[0]+14; point[2].Y = Message[1]-14;
				point[3].X = Message[0]+25; point[3].Y = Message[1]-14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 4:
			{
				point[0].X = Message[0]-25; point[0].Y = Message[1]-12;
				point[1].X = Message[0]-14; point[1].Y = Message[1]-12;
				point[2].X = Message[0]-14; point[2].Y = Message[1]-1;
				point[3].X = Message[0]-25; point[3].Y = Message[1]-1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 5:
			{
				point[0].X = Message[0]-12; point[0].Y = Message[1]-12;
				point[1].X = Message[0]-1; point[1].Y = Message[1]-12;
				point[2].X = Message[0]-1; point[2].Y = Message[1]-1;
				point[3].X = Message[0]-12; point[3].Y = Message[1]-1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 6:
			{
				point[0].X = Message[0]+12; point[0].Y = Message[1]-12;
				point[1].X = Message[0]+1; point[1].Y = Message[1]-12;
				point[2].X = Message[0]+1; point[2].Y = Message[1]-1;
				point[3].X = Message[0]+12; point[3].Y = Message[1]-1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 7:
			{
				point[0].X = Message[0]+25; point[0].Y = Message[1]-12;
				point[1].X = Message[0]+14; point[1].Y = Message[1]-12;
				point[2].X = Message[0]+14; point[2].Y = Message[1]-1;
				point[3].X = Message[0]+25; point[3].Y = Message[1]-1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 8:
			{
				point[0].X = Message[0]-25; point[0].Y = Message[1]+12;
				point[1].X = Message[0]-14; point[1].Y = Message[1]+12;
				point[2].X = Message[0]-14; point[2].Y = Message[1]+1;
				point[3].X = Message[0]-25; point[3].Y = Message[1]+1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 9:
			{
				point[0].X = Message[0]-12; point[0].Y = Message[1]+12;
				point[1].X = Message[0]-1; point[1].Y = Message[1]+12;
				point[2].X = Message[0]-1; point[2].Y = Message[1]+1;
				point[3].X = Message[0]-12; point[3].Y = Message[1]+1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 10:
			{
				point[0].X = Message[0]+12; point[0].Y = Message[1]+12;
				point[1].X = Message[0]+1; point[1].Y = Message[1]+12;
				point[2].X = Message[0]+1; point[2].Y = Message[1]+1;
				point[3].X = Message[0]+12; point[3].Y = Message[1]+1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 11:
			{
				point[0].X = Message[0]+25; point[0].Y = Message[1]+12;
				point[1].X = Message[0]+14; point[1].Y = Message[1]+12;
				point[2].X = Message[0]+14; point[2].Y = Message[1]+1;
				point[3].X = Message[0]+25; point[3].Y = Message[1]+1;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 12:
			{
				point[0].X = Message[0]-25; point[0].Y = Message[1]+25;
				point[1].X = Message[0]-14; point[1].Y = Message[1]+25;
				point[2].X = Message[0]-14; point[2].Y = Message[1]+14;
				point[3].X = Message[0]-25; point[3].Y = Message[1]+14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);;
				break;
			}
			case 13:
			{
				point[0].X = Message[0]-12; point[0].Y = Message[1]+25;
				point[1].X = Message[0]-1; point[1].Y = Message[1]+25;
				point[2].X = Message[0]-1; point[2].Y = Message[1]+14;
				point[3].X = Message[0]-12; point[3].Y = Message[1]+14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 14:
			{
				point[0].X = Message[0]+12; point[0].Y = Message[1]+25;
				point[1].X = Message[0]+1; point[1].Y = Message[1]+25;
				point[2].X = Message[0]+1; point[2].Y = Message[1]+14;
				point[3].X = Message[0]+12; point[3].Y = Message[1]+14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
			case 15:
			{
				point[0].X = Message[0]+25; point[0].Y = Message[1]+25;
				point[1].X = Message[0]+14; point[1].Y = Message[1]+25;
				point[2].X = Message[0]+14; point[2].Y = Message[1]+14;
				point[3].X = Message[0]+25; point[3].Y = Message[1]+14;

				BSP_LCD_SetTextColor(couleur);
				BSP_LCD_FillPolygon(point, 4);
				break;
			}
		}
	}
}

void tourne(uint32_t tableau[])
{
	if ((tableau[1] == tableau[0]+1)&&(tableau[2]==tableau[1]+3)&&(tableau[3]==tableau[2]+1))
	{

	}
	else if ((tableau[0] == tableau[1]+4)&&(tableau[2]==tableau[1]+8)&&(tableau[3]==tableau[2]+4))
	{
		tableau[0]=5;
		tableau[1]=4;
		tableau[2]=6;
		tableau[3]=7;
	}
	else if ((tableau[0] == tableau[1]+1)&&(tableau[2]==tableau[1]+2)&&(tableau[3]==tableau[2]+1))
	{
		tableau[0]=5;
		tableau[1]=1;
		tableau[2]=9;
		tableau[3]=13;
	}
	else
	{
		switch(tableau[0])
		{
			case 1:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]+4;
				}
				break;
			}
			case 4:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]+1;
				}
				break;
			}
			case 2:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]+4;
				}
				break;
			}
			case 7:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]-1;
				}
				break;
			}
			case 11:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]-1;
				}
				break;
			}
			case 14:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]-4;
				}
				break;
			}
			case 8:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]+1;
				}
				break;
			}
			case 13:
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=tableau[i]-4;
				}
				break;
			}
		}

		for (int i = 1; i < 4; ++i)
		{
			switch(tableau[i]+5-tableau[0])
			{
				case 0:
				{
					tableau[i]=2+tableau[0]-5;
					break;
				}
				case 1:
				{
					tableau[i]=6+tableau[0]-5;
					break;
				}
				case 2:
				{
					tableau[i]=10+tableau[0]-5;
					break;
				}
				case 6:
				{
					tableau[i]=9+tableau[0]-5;
					break;
				}
				case 10:
				{
					tableau[i]=8+tableau[0]-5;
					break;
				}
				case 9:
				{
					tableau[i]=4+tableau[0]-5;
					break;
				}
				case 8:
				{
					tableau[i]=0+tableau[0]-5;
					break;
				}
				case 4:
				{
					tableau[i]=1+tableau[0]-5;
					break;
				}
			}
		}
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uint8_t Message[2];
	uint8_t incr_score[2];
	uint8_t Lance_el_svt[2];
	uint8_t endgame[2];
	if(rxbuffer[0]=='a') HAL_GPIO_WritePin(LED16_GPIO_Port, LED16_Pin,1);
	if(rxbuffer[0]=='e') HAL_GPIO_WritePin(LED16_GPIO_Port, LED16_Pin,0);

	HAL_UART_Receive_IT(&huart1,rxbuffer,1);

//	if(rxbuffer[0]=='z') Message[0] = 1;
//	if(rxbuffer[0]=='s') Message[0] = 2;
//	if(rxbuffer[0]=='q') Message[0] = 3;
//	if(rxbuffer[0]=='d') Message[0] = 4;
//	if(rxbuffer[0]==' ') Message[0] = 5;
//
//	xQueueSendFromISR(zqsdHandle, &Message, 0);

	switch (rxbuffer[0])
	{
		case 'z':
		{
			Message[0] = 1;
			xQueueSendFromISR(zqsdHandle, &Message, 0);
			break;
		}
		case 's':
		{
			Message[0] = 2;
			xQueueSendFromISR(zqsdHandle, &Message, 0);
			break;
		}
		case 'q':
		{
			Message[0] = 3;
			xQueueSendFromISR(zqsdHandle, &Message, 0);
			break;
		}
		case 'd':
		{
			Message[0] = 4;
			xQueueSendFromISR(zqsdHandle, &Message, 0);
			break;
		}
		case ' ':
		{
			Message[0] = 5;
			xQueueSendFromISR(zqsdHandle, &Message, 0);
			break;
		}
		case 'r':
		{
			Message[0] = 6;
			xQueueSendFromISR(zqsdHandle, &Message, 0);
			break;
		}
		case '0':
		{
			Lance_el_svt[0] = 0;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case '1':
		{
			Lance_el_svt[0] = 1;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case '2':
		{
			Lance_el_svt[0] = 2;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case '3':
		{
			Lance_el_svt[0] = 3;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case '4':
		{
			Lance_el_svt[0] = 4;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case '5':
		{
			Lance_el_svt[0] = 5;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case '6':
		{
			Lance_el_svt[0] = 6;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case '7':
		{
			Lance_el_svt[0] = 7;
			xQueueSendFromISR(Lance_el_svtHandle, &Lance_el_svt, 0);
			break;
		}
		case 'p':
		{
			endgame[0] = 1;
			xQueueSendFromISR(endgameHandle, &endgame, 0);
			break;
		}
		case 'n':
		{
			incr_score[0] = 5;
			xQueueSendFromISR(incr_scoreHandle, &incr_score, 0);
			break;
		}
	}
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_Affichage_temps */
/**
* @brief Function implementing the myTask04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Affichage_temps */
void Affichage_temps(void const * argument)
{
  /* USER CODE BEGIN Affichage_temps */
	char texte[15]={};
	uint8_t Message[1] = {0};
	RTC_TimeTypeDef time;
	RTC_DateTypeDef date;
	int score = 0;

	MX_RTC_Init();

	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
  /* Infinite loop */
	for(;;)
	{
		HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);

		sprintf(texte, "%d min %d sec", time.Minutes, time.Seconds);
		xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
		BSP_LCD_SetTextColor(LCD_COLOR_RED);
		BSP_LCD_DisplayStringAt(0, LINE(0), texte, LEFT_MODE);
		xSemaphoreGive(MutexEcranHandle);

		xQueueReceive(incr_scoreHandle, &Message, 40);
		score = score + 1 + Message[0]*100;
		Message[0] = 0;

		sprintf(texte, "score %d", score);
		xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
		BSP_LCD_SetTextColor(LCD_COLOR_RED);
		BSP_LCD_DisplayStringAt(0, LINE(1), texte, RIGHT_MODE);
		xSemaphoreGive(MutexEcranHandle);

		vTaskDelayUntil(&xLastWakeTime, 100);
	}
  /* USER CODE END Affichage_temps */
}

/* USER CODE BEGIN Header_Pave */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Pave */
void Pave(void const * argument)
{
  /* USER CODE BEGIN Pave */
	uint8_t MAE = 0;
	uint8_t Lance_el_svt[1] = {0};
	uint8_t Message[2] = {0, 0};
	uint16_t coord[7] = {0,0,0,0,0,0,0};
	uint32_t tableau[4] = {0,0,0,0};
	int couleur;
	int forme;
	char texte[30]={};
	int u, v;

	Point point[4];
	point[0].X = 1; point[0].Y = 1;
	point[1].X = 149; point[1].Y = 1;
	point[2].X = 149; point[2].Y = 269;
	point[3].X = 1; point[3].Y = 269;

	xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
	BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
	BSP_LCD_FillPolygon(point, 4);
	xSemaphoreGive(MutexEcranHandle);

	point[0].X = 281; point[0].Y = 1;
	point[1].X = 478; point[1].Y = 1;
	point[2].X = 478; point[2].Y = 269;
	point[3].X = 281; point[3].Y = 269;

	xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
	BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
	BSP_LCD_FillPolygon(point, 4);
	xSemaphoreGive(MutexEcranHandle);

	point[0].X = 424; point[0].Y = 228;
	point[1].X = 477; point[1].Y = 228;
	point[2].X = 477; point[2].Y = 268;
	point[3].X = 424; point[3].Y = 268;

	xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
	BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
	BSP_LCD_FillPolygon(point, 4);
	xSemaphoreGive(MutexEcranHandle);

	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();

	/* Infinite loop */
	for(;;)
	{
		xQueueReceive(zqsdHandle, &Message, 10);

		if (MAE == 0)
		{
			xSemaphoreTake(MutexelsvtHandle,portMAX_DELAY);
			forme = elsvt[0];
			couleur = elsvt[1];
			xSemaphoreGive(MutexelsvtHandle);

			coord[0] = 215;
			coord[1] = 32;
			coord[6] = couleur;

			switch(forme)
			{
				case 1:
					{
						coord[2] = 1;
						coord[3] = 0;
						coord[4] = 2;
						coord[5] = 3;
						break;
					}
				case 2:
					{
						coord[2] = 1;
						coord[3] = 0;
						coord[4] = 2;
						coord[5] = 5;
						break;
					}
				case 3:
					{
						coord[2] = 1;
						coord[3] = 0;
						coord[4] = 5;
						coord[5] = 6;
						break;
					}
				case 4:
					{
						coord[2] = 1;
						coord[3] = 0;
						coord[4] = 2;
						coord[5] = 6;
						break;
					}
				case 5:
					{
						coord[2] = 1;
						coord[3] = 2;
						coord[4] = 5;
						coord[5] = 6;
						break;
					}
				case 6:
					{
						coord[2] = 1;
						coord[3] = 2;
						coord[4] = 4;
						coord[5] = 5;
						break;
					}
				case 7:
					{
						coord[2] = 1;
						coord[3] = 0;
						coord[4] = 2;
						coord[5] = 4;
						break;
					}
			}

			xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
			Affiche_Pavee(coord[0], coord[1], coord[2], coord[3], coord[4], coord[5], coord[6]);
			xSemaphoreGive(MutexEcranHandle);

			xSemaphoreTake(positiontrucquibougeHandle,portMAX_DELAY);
			for (int i = 0; i < 7; ++i)
			{
				positiontkb[i] = coord[i];
			}
			xSemaphoreGive(positiontrucquibougeHandle);

			xQueueSend(Lance_el_svtHandle, &Lance_el_svt, 0);

			MAE = 1;
		}
		if (MAE == 1)
		{
			xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
			Affiche_Pavee(coord[0], coord[1], coord[2], coord[3], coord[4], coord[5], 0);
			xSemaphoreGive(MutexEcranHandle);

			coord[1] = coord[1]+1;
			if (Message[0] == 1)
			{
				coord[1] = coord[1] - 13;
			}
			if (Message[0] == 2)
			{
				coord[1] = coord[1] + 12;
			}
			if (Message[0] == 3)
			{
				for (int i = 2; i < 6; ++i)
				{
					u = (coord[0]-13-176)/13;
					v = (coord[1]-32)/13;
					u = u + 10*v;
					u = u + coord[i]%4 + 10*(coord[i]/4);
					xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
					if(Black_Board[u]!=0)
					{
						Message[0] = 0;
					}
					xSemaphoreGive(MutextableauHandle);
				}
				if (Message[0] != 0)
				{
					coord[0] = coord[0] - 13;
					Message[0] = 0;
				}
			}
			if (Message[0] == 4)
			{
				for (int i = 2; i < 6; ++i)
				{
					u = (coord[0]+13-176)/13;
					v = (coord[1]-32)/13;
					u = u + 10*v;
					u = u + coord[i]%4 + 10*(coord[i]/4);
					xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
					if(Black_Board[u]!=0)
					{
						Message[0] = 0;
					}
					xSemaphoreGive(MutextableauHandle);
				}
				if (Message[0] != 0)
				{
					coord[0] = coord[0] + 13;
					Message[0] = 0;
				}
			}
			if (Message[0] == 6)
			{
				for (int i = 0; i < 4; ++i)
				{
					tableau[i]=coord[i+2];
				}
				tourne(tableau);
				for (int i = 0; i < 4; ++i)
				{
					u = (coord[0]-176)/13;
					v = (coord[1]-32)/13;
					u = u + 10*v;
					u = u + tableau[i]%4 + 10*(tableau[i]/4);
					xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
					if(Black_Board[u]!=0)
					{
						Message[0] = 0;
					}
					xSemaphoreGive(MutextableauHandle);
				}
				if (Message[0] != 0)
				{
					for (int i = 0; i < 4; ++i)
					{
						coord[i+2]=tableau[i];
					}
					Message[0] = 0;
				}
			}




			if (coord[1] > 240)
			{
				if ((coord[2] < 12)&&(coord[3] < 12)&&(coord[4] < 12)&&(coord[5] < 12))
				{
					coord[2] = coord[2]+4;
					coord[3] = coord[3]+4;
					coord[4] = coord[4]+4;
					coord[5] = coord[5]+4;
					coord[1] = 238-13;
				}
				else
				{
					for (int i = 2; i < 6; ++i)
					{
						u = (coord[0]-176)/13;
						v = (coord[1]-32)/13;
						u = u + 10*v;
						u = u + coord[i]%4 + 10*(coord[i]/4);
						xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
						Black_Board[u] = coord[6];
						xSemaphoreGive(MutextableauHandle);
					}
					coord[1] = 240;
					MAE = 0;
				}
			}
			if (MAE == 1)
			{
				for (int i = 2; i < 6; ++i)
				{
					u = (coord[0]-176)/13;
					v = (coord[1]-32)/13;
					u = u + 10*v;
					u = u + coord[i]%4 + 10*(coord[i]/4);
					xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
					if (Black_Board[u+10] != 0)
					{
						MAE = 0;
					}
//					sprintf(texte, "direction: %d %d", u, Black_Board[u]);
					xSemaphoreGive(MutextableauHandle);
//					xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
//					BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//					BSP_LCD_DisplayStringAt(1, LINE(5+i), texte, RIGHT_MODE);
//					xSemaphoreGive(MutexEcranHandle);
				}
				if (MAE == 0)
				{
					for (int i = 2; i < 6; ++i)
					{
						u = (coord[0]-176)/13;
						v = (coord[1]-32)/13;
						u = u + 10*v;
						u = u + coord[i]%4 + 10*(coord[i]/4);
						xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
						Black_Board[u] = coord[6];
						xSemaphoreGive(MutextableauHandle);
					}
					coord[0] = coord[0] - ((coord[0]-176)%13);
					coord[1] = coord[1] - ((coord[1]-32)%13);
				}
			}



			if (coord[0] < 176)
			{
				if (!((((coord[2])%4) == 0)||(((coord[3])%4) == 0)||(((coord[4])%4) == 0)||(((coord[5])%4) == 0)))
				{
					coord[2] = coord[2]-1;
					coord[3] = coord[3]-1;
					coord[4] = coord[4]-1;
					coord[5] = coord[5]-1;
				}
				coord[0] = 176;
			}
			if (coord[0] > 254)
			{
				if (!((((coord[2]+1)%4) == 0)||(((coord[3]+1)%4) == 0)||(((coord[4]+1)%4) == 0)||(((coord[5]+1)%4) == 0)))
				{
					coord[2] = coord[2]+1;
					coord[3] = coord[3]+1;
					coord[4] = coord[4]+1;
					coord[5] = coord[5]+1;
				}
				coord[0] = 254;
			}
			if (coord[1] < 32)
			{
				if (((!(coord[2] < 4))&&(coord[3] > 4))&&((coord[4] > 4)&&(coord[5] > 4)))
				{
					coord[2] = coord[2]-4;
					coord[3] = coord[3]-4;
					coord[4] = coord[4]-4;
					coord[5] = coord[5]-4;
					coord[1] = 32+13;
				}
				else
				{
					coord[1] = 32;
				}
			}

			xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
			Affiche_Pavee(coord[0], coord[1], coord[2], coord[3], coord[4], coord[5], coord[6]);
			xSemaphoreGive(MutexEcranHandle);

			xSemaphoreTake(positiontrucquibougeHandle,portMAX_DELAY);
			for (int i = 0; i < 7; ++i)
			{
				positiontkb[i] = coord[i];
			}
			xSemaphoreGive(positiontrucquibougeHandle);
		}

		vTaskDelayUntil(&xLastWakeTime, 50);
	}
  /* USER CODE END Pave */
}

/* USER CODE BEGIN Header_El_suivant */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_El_suivant */
void El_suivant(void const * argument)
{
  /* USER CODE BEGIN El_suivant */
	int couleur;
	int forme;
	TickType_t xLastWakeTime;
	uint8_t Message[1] ={0};
	xLastWakeTime = xTaskGetTickCount();
	Point point[4];
	point[0].X = 424; point[0].Y = 228;
	point[1].X = 477; point[1].Y = 228;
	point[2].X = 477; point[2].Y = 268;
	point[3].X = 424; point[3].Y = 268;

  /* Infinite loop */
	for(;;)
	{
		xQueueReceive(Lance_el_svtHandle, &Message, portMAX_DELAY);
		if (Message[0] == 0)
		{
			forme = rand() % 7 + 1;
		}
		else
		{
			forme = Message[0];
		}
		couleur = rand() % 5 + 1;

		xSemaphoreTake(MutexelsvtHandle,portMAX_DELAY);
		elsvt[0] = forme;
		elsvt[1] = couleur;
		xSemaphoreGive(MutexelsvtHandle);

		xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
		BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
		BSP_LCD_FillPolygon(point, 4);
		xSemaphoreGive(MutexEcranHandle);

		switch(forme)
		{
			case 1:
				{
					xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
					Affiche_Pavee(451, 242, 4, 5, 6, 7, couleur);
					xSemaphoreGive(MutexEcranHandle);
					break;
				}
			case 2:
				{
					xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
					Affiche_Pavee(451, 242, 4, 5, 6, 9, couleur);
					xSemaphoreGive(MutexEcranHandle);
					break;
				}
			case 3:
				{
					xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
					Affiche_Pavee(451, 242, 4, 5, 9, 10, couleur);
					xSemaphoreGive(MutexEcranHandle);
					break;
				}
			case 4:
				{
					xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
					Affiche_Pavee(451, 242, 4, 5, 6, 10, couleur);
					xSemaphoreGive(MutexEcranHandle);
					break;
				}
			case 5:
				{
					xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
					Affiche_Pavee(451, 242, 5, 6, 9, 10, couleur);
					xSemaphoreGive(MutexEcranHandle);
					break;
				}
			case 6:
			{
				xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
				Affiche_Pavee(451, 242, 5, 6, 8, 9, couleur);
				xSemaphoreGive(MutexEcranHandle);
				break;
			}
			case 7:
			{
				xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
				Affiche_Pavee(451, 242, 5, 4, 6, 8, couleur);
				xSemaphoreGive(MutexEcranHandle);
				break;
			}
		}

		vTaskDelayUntil(&xLastWakeTime, 10);
	}
  /* USER CODE END El_suivant */
}

/* USER CODE BEGIN Header_GameOver */
/**
* @brief Function implementing the myTask07 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_GameOver */
void GameOver(void const * argument)
{
  /* USER CODE BEGIN GameOver */
	char texte[15]={};
	uint8_t Message[1] ={0};
	uint8_t MAE = 0;

	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
  /* Infinite loop */
	for(;;)
	{
		xQueueReceive(endgameHandle, &Message, portMAX_DELAY);
//		//
//		taskENTER_CRITICAL( );
//		for(int l = 0; l < 200; ++l)
//		{
//			sprintf(texte, "%d", Black_Board[l]);
//			BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
//			BSP_LCD_DisplayStringAt(10*(l%10), LINE(1 + l/10), texte, LEFT_MODE);
//		}
//		taskEXIT_CRITICAL( );
//		//

		taskENTER_CRITICAL( );
		Message[0] = 0;

		if (MAE == 0)
		{
			if( myTask04Handle != NULL )
			{
				vTaskDelete( myTask04Handle );
			}
			if( myTask03Handle != NULL )
			{
				vTaskDelete( myTask03Handle );
			}
			if( myTask02Handle != NULL )
			{
				vTaskDelete( myTask02Handle );
			}
//			if( myTask08Handle != NULL )
//			{
//				vTaskDelete( myTask08Handle );
//			}
			if( myTask09Handle != NULL )
			{
				vTaskDelete( myTask09Handle );
			}

			vSemaphoreDelete(MutexEcranHandle);
			vSemaphoreDelete(MutextableauHandle);
			vSemaphoreDelete(positiontrucquibougeHandle);
			vSemaphoreDelete(MutexelsvtHandle);


			BSP_LCD_Clear(LCD_COLOR_LIGHTGREEN);

			sprintf(texte, "GAME OVER");
			BSP_LCD_SetTextColor(LCD_COLOR_RED);
			BSP_LCD_DisplayStringAt(200, LINE(10), texte, RIGHT_MODE);

			for (int i = 0; i < 200; ++i)
			{
				Black_Board[i] = 0;
			}

			MAE = 1;
		}
		else if (MAE == 1)
		{
			BSP_LCD_Clear(LCD_COLOR_LIGHTGREEN);

			/* definition and creation of MutexEcran */
			osMutexDef(MutexEcran);
			MutexEcranHandle = osMutexCreate(osMutex(MutexEcran));

			/* definition and creation of Mutextableau */
			osMutexDef(Mutextableau);
			MutextableauHandle = osMutexCreate(osMutex(Mutextableau));

			/* definition and creation of positiontrucquibouge */
			osMutexDef(positiontrucquibouge);
			positiontrucquibougeHandle = osMutexCreate(osMutex(positiontrucquibouge));

			/* definition and creation of Mutexelsvt */
			osMutexDef(Mutexelsvt);
			MutexelsvtHandle = osMutexCreate(osMutex(Mutexelsvt));

			/* definition and creation of myTask04 */
			osThreadDef(myTask04, Affichage_temps, osPriorityLow, 0, 1024);
			myTask04Handle = osThreadCreate(osThread(myTask04), NULL);

//			/* definition and creation of myTask08 */
// 			osThreadDef(myTask08, Joystick, osPriorityIdle, 0, 128);
//			myTask08Handle = osThreadCreate(osThread(myTask08), NULL);

			/* definition and creation of myTask02 */
			osThreadDef(myTask02, Pave, osPriorityNormal, 0, 1500);
			myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

			/* definition and creation of myTask03 */
			osThreadDef(myTask03, El_suivant, osPriorityLow, 0, 512);
			myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

			/* definition and creation of myTask09 */
			osThreadDef(myTask09, Effacelignes, osPriorityBelowNormal, 0, 1024);
			myTask09Handle = osThreadCreate(osThread(myTask09), NULL);

			MAE = 0;
		}
		taskEXIT_CRITICAL( );
		vTaskDelayUntil(&xLastWakeTime, 1000);
	}
  /* USER CODE END GameOver */
}

/* USER CODE BEGIN Header_Joystick */
/**
* @brief Function implementing the myTask08 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Joystick */
void Joystick(void const * argument)
{
  /* USER CODE BEGIN Joystick */
	int32_t stick_h = 0, stick_v = 0;
	char texte[30]={};
	uint8_t Message[1] ={0};
	uint8_t MAE = 0;

	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;

	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
  /* Infinite loop */
  for(;;)
  {
		sConfig.Channel = ADC_CHANNEL_8;
		HAL_ADC_ConfigChannel(&hadc3, &sConfig);
		HAL_ADC_Start(&hadc3);
		while(HAL_ADC_PollForConversion(&hadc3, 100)!=HAL_OK);
		stick_v = HAL_ADC_GetValue(&hadc3);

		//sConfig.Channel = ADC_CHANNEL_8;
		//HAL_ADC_ConfigChannel(&hadc1, &sConfig);
		HAL_ADC_Start(&hadc1);
		while(HAL_ADC_PollForConversion(&hadc1, 100)!=HAL_OK);
		stick_h = HAL_ADC_GetValue(&hadc1);

		sprintf(texte, "h: %d v: %d ", stick_h, stick_v);
		xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
		BSP_LCD_DisplayStringAt(1, LINE(0), texte, RIGHT_MODE);
		xSemaphoreGive(MutexEcranHandle);

		if ((stick_v < 3500)&&(stick_v > 500)&&(stick_h < 3500)&&(stick_h > 500)&&(HAL_GPIO_ReadPin(BP_3_GPIO_Port,BP_3_Pin)))
		{
			Message[0] = 0;
			MAE = 0;
		}
		if (MAE==0)
		{
			if (stick_v > 3500)
			{
				Message[0] = 1;
				xQueueSend(zqsdHandle, &Message, 0);
				MAE = 4;
			}
			if (stick_v < 500)
			{
				Message[0] = 2;
				xQueueSend(zqsdHandle, &Message, 0);
				MAE = 4;
			}
			if (stick_h > 3500)
			{
				Message[0] = 3;
				xQueueSend(zqsdHandle, &Message, 0);
				MAE = 2;
			}
			if (stick_h < 500)
			{
				Message[0] = 4;
				xQueueSend(zqsdHandle, &Message, 0);
				MAE = 2;
			}
			if (!HAL_GPIO_ReadPin(BP_3_GPIO_Port,BP_3_Pin))
			{
				Message[0] = 6;
				xQueueSend(zqsdHandle, &Message, 0);
				MAE = 4;
			}
		}

		Message[0] = 0;

		if (MAE>0)
		{
			MAE=MAE-1;
		}

		if (!HAL_GPIO_ReadPin(BP1_GPIO_Port,BP1_Pin))
		{
			Message[0] = 1;
			xQueueSendFromISR(endgameHandle, &Message, 0);
		}

		vTaskDelayUntil(&xLastWakeTime, 100);
  }
  /* USER CODE END Joystick */
}

/* USER CODE BEGIN Header_Effacelignes */
/**
* @brief Function implementing the myTask09 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Effacelignes */
void Effacelignes(void const * argument)
{
  /* USER CODE BEGIN Effacelignes */
	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
	Point point[4];
	uint8_t memoire;
	uint32_t couleur = 0;
	int condition = 1;
	int toto;
	uint8_t incr_score[1];
  /* Infinite loop */
	for(;;)
	{
		for (int i = 0; i < 20; ++i)
		{
			int j = 0;
			condition = 1;
			while ((j < 10)&&condition)
			{
				xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
				memoire = Black_Board[10*(19-i) + j];
				xSemaphoreGive(MutextableauHandle);
				if (memoire == 0)
				{
					condition = 0;
				}
				j = j+1;
			}
			if (condition == 1)
			{
				incr_score[0] = i;
				xQueueSend(incr_scoreHandle, &incr_score, 0);
				point[0].X = 151; point[0].Y = 5+(19-i)*13;
				point[1].X = 280; point[1].Y = 5+(19-i)*13;
				point[2].X = 280; point[2].Y = 18+(19-i)*13;
				point[3].X = 151; point[3].Y = 18+(19-i)*13;

				xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
				BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGREEN);
				BSP_LCD_FillPolygon(point, 4);
				xSemaphoreGive(MutexEcranHandle);

				for(int k = 0; k < 10; ++k)
				{
					xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
					Black_Board[10*(19-i)+k]=0;
					xSemaphoreGive(MutextableauHandle);
				}

				for(int j = i; j < 20; ++j)
				{
					for(int k = 0; k < 10; ++k)
					{
						toto = 10*(19-j) + k;
						xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
						memoire = Black_Board[toto];
						xSemaphoreGive(MutextableauHandle);
						if (memoire != 0)
						{
							xSemaphoreTake(MutextableauHandle,portMAX_DELAY);
							if (toto+10<200)
							{
								Black_Board[toto + 10] = memoire;
							}
							Black_Board[toto] = 0;
							xSemaphoreGive(MutextableauHandle);

							point[0].X = 151+13*(toto%10); point[0].Y = 5+13*(toto/10);
							point[1].X = 163+13*(toto%10); point[1].Y = 5+13*(toto/10);
							point[2].X = 163+13*(toto%10); point[2].Y = 17+13*(toto/10);
							point[3].X = 151+13*(toto%10); point[3].Y = 17+13*(toto/10);

							xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
							BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGREEN);
							BSP_LCD_FillPolygon(point, 4);
							xSemaphoreGive(MutexEcranHandle);

							toto = toto+10;
							if (toto<200)
							{
								point[0].X = 151+13*(toto%10); point[0].Y = 7+13*(toto/10);
								point[1].X = 162+13*(toto%10); point[1].Y = 7+13*(toto/10);
								point[2].X = 162+13*(toto%10); point[2].Y = 18+13*(toto/10);
								point[3].X = 151+13*(toto%10); point[3].Y = 18+13*(toto/10);

								switch(memoire)
								{
									case 1:
										{
											couleur = LCD_COLOR_BLUE;
											break;
										}
									case 2:
										{
											couleur = LCD_COLOR_MAGENTA;
											break;
										}
									case 3:
										{
											couleur = LCD_COLOR_DARKCYAN;
											break;
										}
									case 4:
										{
											couleur = LCD_COLOR_ORANGE;
											break;
										}
									case 5:
										{
											couleur = LCD_COLOR_RED;
											break;
										}
								}

								xSemaphoreTake(MutexEcranHandle,portMAX_DELAY);
								BSP_LCD_SetTextColor(couleur);
								BSP_LCD_FillPolygon(point, 4);
								xSemaphoreGive(MutexEcranHandle);
							}
						}
					}
				}
			}
		}
		vTaskDelayUntil(&xLastWakeTime, 200);
	}
  /* USER CODE END Effacelignes */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
