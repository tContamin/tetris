# Bienvenu dans la partie graphique

Les graphiques ont relativement peu évolué avec les différentes versions de Tetris. Normalement, chaque Tetrominos est supposé avoir une couleur qui lui est propre.

<center>
    ![Les 7 Tetrominos](img/tetrominos.png "Tetrominos")
</center>

J'ai pris quelques libertés par rapport à ces dernières puisques les couleurs sont aléatoires:

* bleu
* magenta
* bleu foncé
* orange
* rouge

## Le terrain de jeu

La zone de jeu ressemble à ce qui suit

<center>
    <div style="width:75%">![écran d'une partie sur la carte](img/monjeu3.jpg "Hail Tetris")</div>
</center>

Le terrain se décompose en 4 parties. Un rectangle bleu à gauche de l'écran, la zone de jeu au centre, un autre rectangle bleu délimitant la zone de jeu à droite et en bas à droite une zone réservée à la postérité.

En haut à droite, en rouge, est calculé le score. Juste au dessus, on peut observer la position du joystick qui sert principalement à dépanner la carte. En haut à gauche est affiché un timer. En bas à droit, la pièce suivante que le joueur pourra mouvoir et au centre de l'écran, la zone de jeu. Un pavé de 10 carreaux par 20 carreaux, c'est le terrain de jeu reglementaire d'un Tetris. Les différentes couleurs sont celles qui s'affichent en bas à gauche de la zone de jeu sur l'image.

Les blocs apparaissent en haut de l'écran, au milieu. Lorsqu'un bloc est installé, celui en bas à droite vient prendre place en haut de l'écran en lieu et place du bloc de couleur bleu (oui ce bleu ressemble à du gris). Un nouveau bloc est généré suivant une horloge pseudo aléatoire puisque basée sur l'horloge de démarage de la carte.

        couleur = rand() % 5 + 1;
        #ce code génère un entier compris entre 1 et 5

## L'affichage à proprement parler

Pour bien comprendre ce qui a été mis en place, il faut savoir que 6 taches composent cette partie du projet:

* <font color="red">Affichage_temps</font>
* <font color="red">Pave</font>
* <font color="red">El_suivant</font>
* <font color="red">GameOver</font>
* <font color="red">Effacelignes</font>

Mais ce qui va nous intéresser particulièrement ici, ce sont les fonctions <font color="magenta">tourne</font> et <font color="magenta">Affiche_Pavee</font>.

## Tetromino descendant

L'affichage du Tetromino descendant est géré à chaque appel de la fonction <li><font color="magenta">Affiche_Pavee</font></li> qui nécessite 7 arguments. Ce que fait la fonction, c'est circuler au sein d'un carré fictif qui ressemble à ce qui suit.

<center>
    ![Carre fictif servant a l'affichage numéroté de 1 à 16](img/carre.png "Carre fictif servant a l'affichage")
</center>

Un Tetromino est désigné par 7 arguments. La position en pixels du centre du carré fictif représenté ci dessus (2 arguments). Les numéros des 4 cases qui composent le Tetromino (4 arguments) et enfin la couleur du Tetromino. L'affichage se fait ensuite avec l'utilisation successive de  <i>BSP_LCD_FillPolygon(point, couleur)</i>. Avec "point" contenant les coordonnées du carré à afficher calculées à partir des arguments en entrée de la fonction.

Lors de l'exécution de la fonction, on remarque que les carrés qui composent le Tetromino semblent clignoter. C'est due à l'utilisation de BSP_LCD_FillPolygon au lieu de BSP_LCD_FillRectangle. En effet, le schéma de remplissage est le suivant

<center>
    ![Remplissage d'un carré par étape avec la fonction BSP_LCD_FillPolygon](img/cube.png "Remplissage d'un carré par étape avec la fonction BSP_LCD_FillPolygon")
</center>

On aurait pu éviter ce clignotement en utilisant la fonction BSP_LCD_FillRectangle mais je l'ai trouvé adapté pour mettre en valeur le Tetromino descendant. Par ailleurs il donne un style un peu rétro au gameplay ce qui n'est pas malvenu.

## Affichage du Tetromino descendant dans le temps

A chaque exécution de la tache Pave, le Tetromino est en premier lieu effacé (on utilise <font color="magenta">Affiche_Pavee</font>pour afficher un Tetromino de la couleur du fond de l'écran -"LIGTH_GREEN"-). Puis réaffiché avec une descente de 10 ou 1 pixel suivant si la descente rapide est activée ou non.

Bien évidemment, avant de faire descendre le Tetromino, on s'assure que la case du dessous n'est pas occupée en vérifiant rapidement <font color="blue">Black_Board</font>. Si c'est le cas, on positionne correctement notre Tetromino et on remplit <font color="blue">Black_Board</font> puis on lance <font color="red">El_suivant</font> via <font color="green">Lance_el_svt</font>.

Si on arrive en bas de l'écran, plutôt que de déplacer le centre du Tetromino, on déplace les blocs vers le bas et on remonte éventuellement le centre du carré pour garder une position cohérente. Par exemple, les coordonnées [y, 240, 1, 2, 3, 4, couleur] deviendront [y, 228, 5, 6, 7, 8, couleur] au coup suivant.

## Déplacer le Tetromino sur le coté

On effectue un premier test pour vérifier que le déplacement est valide. Il ne doit pas y avoir de valeur dans le <font color="blue">Black_Board</font> adjacent à l'un des blocs du Tetromino et aucun bloc du Tetromino ne doit se trouver à coté du bord du terrain de jeu.

Si le déplacement est valide, on effectue un second test pour savoir si le carré fictif n'est pas au bord du terrain de jeu. Si c'est le cas, on va déplacer les blocs au sein du carré fictif ([1, 5, 9, 13] va devenir [2, 6, 10, 14] par exemple mais x et y, les coordonnées du centre du carré fictif ne bougeront pas). Sinon, on déplace l'abcisse du centre du carré ficitf de 13 pixels dans la diirection indiquée (chaque bloc fait 12 pixels de large et ils sont séparés par 1 pixels).

<center>
    ![Dimensions du carré fictif](img/pix.png "Dimensions du carré fictif")
</center>

## Faire tourner le Tetromino

L'intéret du carré fictif intervient ici. J'utilise la fonction tourne à qui je fournie les 6 premières coordonnées de mon Tetromino.

Si le Tetromino est le carré, il n'y pas de rotation.

<center>
    <div style="width:75%">![Tetromino carré](img/bloccarre.png "Bloc carré")</div>
</center>

Si le Tetromino est la barre, il suffit de remplacer par [y, x, 5, 6, 7, 8, couleur] ou par [y, x, 2, 6, 10, 14, couleur].

<center>
    <div style="width:75%">![Tetromino barre](img/blocbarre.png "Bloc barre")</div>
</center>

Sinon, la première coordonnée est celle de mon centre de rotation. La fonction tourne commence par faire une translation de tous les blocs du carré fictif vers le bloc au centre du carré fictif le plus proche. Les blocs au centre étant les blocs numéro 6, 7, 10 et 11. Il suffit d'ajouter ou de retrancher 4 ou 1 à chaque bloc (on peut également avoir 3 ou 5 par combinaison).

Tous les blocs sont alors autour d'un carré de 3 par 3. Il me suffit alors d'appliquer la rotation horaire qui a été mémorisée avec un switch et le tour est joué. Pour éviter d'écrire plusieurs switch, j'ai trouvé une relation linéaire entre les 4 rotations.

<center>
    <div style="width:75%">![Position initiale](img/Image1.png "Position initiale")</div>
</center>

<center>
    <div style="width:75%">![Aprés translation](img/Image2.png "Aprés translation")</div>
</center>

<center>
    <div style="width:75%">![Aprés rotation](img/Image3.png "Aprés rotation")</div>
</center>

## Reste de l'affichage

Le reste de l'affichage n'est pas trés compliqué, la tache <font color="red">Affichage_temps</font> fait appel à un <i>BSP_LCD_DisplayStringAt</i> deux fois et la tache <font color="red">El_suivant</font> fait intervenir <font color="magenta">Affiche_Pavee</font> avec des coordonnées prédéfinies de x et de y comme on pouvait s'en douter. Quand à <font color="red">GameOver</font>, il fait intervenir uniquement un <i>BSP_LCD_Clear(LCD_COLOR_LIGHTGREEN)</i> et un appel de <i>BSP_LCD_DisplayStringAt</i> pour afficher "Game Over". En effet, lorsqu'il regénère les taches, elles exécutent d'elles même les routines d'affichage lors de leur démarage.

La dernière astuce se trouve dans <font color="red">Effacelignes</font>. Cette fonction doit effacer une ligne (appel d'un seul <i>BSP_LCD_FillPolygon(point, couleur)</i> pour toute la ligne afin de gagner en efficacité). En parallèle, elle doit remplacer par des zéros les valeurs du <font color="blue">Black_Board</font>. Ensuite, elle fait descendre les valeurs dans le tableau et elle affiche un par un les blocs à la place qui leur est due après avoir effacé leur instance précédente.

