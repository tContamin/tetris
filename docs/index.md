# Bienvenu dans le monde de Tetris

<center>
    <img src="img/tetris.png" alt="In the beggining, there was Tetris" title="In the beggining, there was Tetris"/>
    [In the beggining, there was Tetris](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
</center>

## Introduction

Le célèbre jeu Tetris a été conçu en 1984 par [Alekseï Pajitnov](https://fr.wikipedia.org/wiki/Alekse%C3%AF_Pajitnov) pour un Electronika-60 (à gauche sur l'image).

<center>
    <img src="img/electroika.png" alt="Premiers pc et consoles (Electronika60, DVK-4 et Macintosh LC 520 de gauche à droite)" title="Electronika60, DVK-4 et Macintosh LC 520 de gauche à droite"/>
</center>

<center>
    [Premiers pc et consoles](http://sputnikimages.com/story/list_123407685/5609692.html)
</center>

<center>
    (Electronika60, DVK-4 et Macintosh LC 520 de gauche à droite)
</center>


## Principe

Tetris met le joueur au défi de réaliser des lignes complètes en déplaçant des pièces de formes différentes, les tétrominos, qui défilent depuis le haut jusqu'au bas de l'écran. Les lignes complétées disparaissent tout en rapportant des points et le joueur peut de nouveau remplir les cases libérées. Le jeu n'a pas de fin : le joueur perd la partie lorsqu'un tétromino reste bloqué en haut. Il doit donc résister le plus longtemps à la chute continue des tétrominos, afin de réaliser le meilleur score. Dans le cas d'une partie à plusieurs, le but est de résister plus longtemps que son adversaire, sachant qu'il est possible de s'envoyer des malus en réalisant des combinaisons de lignes. Selon le journaliste Bill Kunkel, « Tetris répond parfaitement à la définition du meilleur en matière de jeu : une minute pour l'apprendre, une vie entière pour le maîtriser ».

Tetris est l'un des pilliers des jeux vidéos rétro, avec  Pong, Space Invaders, Mario et Pac-Man, c'est l'un des pioniers qui a relancé l'industrie du jeux vidéo après le crash de 1983. Avec le temps, il n'a pas pris une ride.

## Détails

Il existe 7 types de tétrominos différents:

<center>
    <img src="img/tetrominos.png" alt="Tetrominos" title="Tetrominos"/>
</center>

## Meilleure stratégie

Le nombre de points gagné est proportionnel aux nombre de lignes éliminées d'un coup. La meilleure stratégie consiste donc a attendre le tetrominos verticale et à l'insérer pour éliminer 4 lignes d'un coup comme sur l'image ci dessous.

<center>
    <img src="img/verti.png" alt="optimiser score" width="300" title="Optimiser son Score"/>
</center>

## un mot sur la musique

L'une des spécificités du jeu Tetris est sa musique. Le jeu ayant été programmé par un Russe et le jeu ayant parut en pleine guerre froide, l'héritage soviétique est trés marqué. Sur la première édition (celle ci n'est jamais arrivée jusqu'aux US), on peut voir le Kremelin de Moscou. La musique a également de fortes conotations.

La musique originale est [Korobeiniki](https://www.youtube.com/watch?v=lIhGo6GwlCY) de Nikolaï Alekseïevitch Nekrassov écrite en 1861. Elle a été réinterprétée par Nitendo pour correspondre à sa version 8-bits et devient par l'occasion le [refrain entêtant](https://www.youtube.com/watch?v=hc9ATIE17GA) que nous connaissons tous.

## Partie infinie

Le système de jeu de Tetris a été examiné par plusieurs chercheurs. Une question régulièrement abordée est celle portant sur la possibilité d'une partie infinie, notamment par John Brzustowski dans un mémoire de master en 1992. L'étudiant prouve au long de son mémoire qu'une partie de Tetris peut être infinie si l'aléa du jeu est clément avec le joueur. Il existe une suite de pièces qui, quel que soit l'agencement choisi par le joueur, mène à la fin de la partie. Par exemple, les tétriminos S et Z seuls ne permettent pas de réaliser de lignes et obligent le joueur à laisser des espaces vides à chaque ligne31. Avec une séquence suffisamment longue de S et de Z, le joueur se retrouve alors bloqué et la partie se termine.

En effet, si le choix de la pièce suivante est aléatoire, il existe une séquence de S et de Z suffisamment longue pour remplir le tableau. Un joueur peut placer jusqu'à 150 tétriminos S ou Z avant de perdren 2. En pratique, ce scénario n'arrive jamais, car la probabilité d'avoir une suite de 150 pièces S ou Z est de ( 2 / 7 ) 150 {\displaystyle (2/7)^{150}} (2/7)^{{150}}.

<center>
    <img src="img/Tetris_with_only_kinks.gif" alt="Partie avec uniquement des S et des Z" width="300" title="Partie uniquement avec des S et des Z"/>
</center>

## Contenu du dépot

 * Partie graphique
 * Partie sonore
 * Interractions entre les taches

## Le mot de la fin
Tetris a été une source d'inspiration pour nombre de personnes. Qui n'a jamais testé ses compétences en Tetris en remplissant une valise ou le coffre d'une voiture avant un départ en vacance. Ce qu'il faut retenir de Tetris, c'est son incroyable longévité.

<center>
    <img src="img/imageldp.jpg" alt="Hail Tetris" width="300" title="Hail Tetris"/>
</center>
