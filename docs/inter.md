# Bienvenu dans la partie interractions entre les taches

<center>
    ![Schéma présentant les interactions entre les taches](img/schema.png "Schéma présentant les interactions entre les taches")
</center>

## Décompte des éléments

Le projet comporte

* 8 taches
    * <font color="red">StartDefaultTask</font> (inutilisée)
    * <font color="red">Affichage_temps</font>
    * <font color="red">Pave</font>
    * <font color="red">El_suivant</font>
    * <font color="red">GameOver</font>
    * <font color="red">Joystick</font>
    * <font color="red">Effacelignes</font>
    * <font color="red">musica</font>
* 3 fonctions
    * <font color="magenta">periode</font>
    * <font color="magenta">tourne</font>
    * <font color="magenta">Affiche_Pavee</font>
* 2 fonctions d'interruptions
    * <font color="grey">HAL_GPIO_EXTI_Callback</font>
    * <font color="grey">HAL_UART_RxCpltCallback</font>
* 5 Messageries
    * <font color="green">zqsd</font>
    * <font color="green">Lance_el_svt</font>
    * <font color="green">incr_score</font>
    * <font color="green">endgame</font>
    * <font color="green">joystick</font> (inutilisé)
* 4 Mutex
    * <font color="orange">MutexEcran</font>
    * <font color="orange">Mutextableau</font>
    * <font color="orange">positiontrucquibouge</font> (utilisé uniquement par <font color="red">Pave</font>, donc inutile)
    * <font color="orange">Mutexelsvt</font>
* 10 variables globales
    * <font color="blue">rxbuffer[10]</font>
    * <font color="blue">positiontkb[7]</font> (aurait pu être une variable locale)
    * <font color="blue">elsvt[2]</font>
    * <font color="blue">Black_Board[200]</font>
    * <font color="blue">music_on</font>
    * <font color="blue">pointeur</font>
    * <font color="blue">longueur_partition_tetris</font>
    * <font color="blue">tetris[192]</font>
    * <font color="blue">sinus10bit[360]</font>
