# Bienvenu dans la partie interractions entre les taches

<center>
    ![Schéma présentant les interactions entre les taches](img/schema.png "Schéma présentant les interactions entre les taches")
</center>

## Les taches
### <font color="red">Affichage_temps</font>
Cette tache sert à incrémenter le score ainsi que l'horloge.

Elle interragit avec <font color="red">Effacelignes</font> via <font color="green">incr_score</font>.

<i>Elle se lance toutes les 100ms.</i>

### <font color="red">Pave</font>
Cette tache gère les mouvements et l'Affichage du Tetromino qui descend.

Elle interragit avec <font color="magenta">Affiche_Pavee</font>, <font color="magenta">tourne</font>, <font color="orange">MutexEcran</font>, <font color="orange">Mutexelsvt</font>, <font color="orange">positiontrucquibouge</font>, <font color="orange">Mutextableau</font>, <font color="green">zqsd</font>, <font color="green">Lance_el_svt</font>, <font color="blue">elsvt</font>, <font color="blue">positiontkb</font>, <font color="blue">Black_Board</font>

<i>Elle se lance toutes les 50ms.</i>

### <font color="red">El_suivant</font>
Cette tache génère le nouvel élément pseudo-aléatoirement et l'affiche en bas à gauche de l'écran via <font color="magenta">Affiche_Pavee</font>.

Elle interragit avec <font color="green">Lance_el_svt</font>, <font color="blue">elsvt</font>, <font color="orange">MutexEcran</font> et <font color="magenta">Affiche_Pavee</font>

<i>Elle se lance toutes les 10ms mais en pratique elle attend <font color="green">Lance_el_svt</font>.</i>

### <font color="red">GameOver</font>
Cette tache efface toutes les autres taches (sauf musica) ainsi que les Mutex puis les relance. Elle fonctionne via messagerie mais la messagerie doit être chargée via interruption puisque les autres taches sont mortes à un instant donné. Cette tache réinitialise également les variables aléatoires qui peuvent avoir été modifiées. (Rq: pas besoin de modifier le score puisque celui ci est définit en variable globale)

Elle interragit avec toutes les autre <font color="red">taches</font> directement (sauf musica) et tous les <font color="orange">Mutex</font> ainsi qu'avec <font color="blue">Black_Board[200]</font> et <font color="green">endgame</font>.

<i>Elle se lance toutes les 10ms mais en pratique elle attend <font color="green">endgame</font>.</i>

### <font color="red">Joystick</font>
Cette tache lit les données du joystick (bouton pression et potentiomètres) et communique une direction ou la rotation à <font color="red">Pave</font> via <font color="green">zqsd</font>. Dans l'idéal, cette fonction aurait été une interruption, la periodicité de cette tache entraine parfois des désagréments dans le jeu.

Elle interragit avec le joystick et <font color="green">zqsd</font>.

<i>Elle se lance toutes les 100ms.</i>

### <font color="red">Effacelignes</font>
Cette tache lit les données du BlackBoard et le vide si une ligne est pleine tout en gérant l'affichage succinct. Elle communique également avec <font color="red">Affichage_temps</font> via <font color="green">incr_score</font> et incrémente le score au passage. C'est également elle qui lance <font color="red">GameOver</font> si besoin.

Elle interragit avec <font color="blue">Black_Board</font>, <font color="orange">Mutextableau</font>, <font color="green">incr_score</font> et <font color="orange">MutexEcran</font>.

<i>Elle se lance toutes les 200ms.</i>

### <font color="red">musica</font>
Cette tache se situe un peu à part dans le sens ou elle n'interragit pas avec les autres, elle se contente de lire la musique. La partition se trouve dans <font color="blue">tetris</font> et les valeurs prises par le DAC dans <font color="blue">sinus10bit</font>. Elle utilise un DMA pour ne pas être appellée trop régulièrement. Son déclenchement ou son arrêt se fait uniquement via <font color="blue">music_on</font> dont la valeur est modifiée par interruption via <font color="grey">HAL_GPIO_EXTI_Callback</font> appelée pour le bouton 2.

Elle interragit avec le DMA, <font color="blue">music_on</font>, <font color="blue">pointeur</font>, <font color="blue">longueur_partition_tetris</font>, <font color="blue">tetris</font> et <font color="blue">sinus10bit</font>.

<i>Elle se lance toutes les 150ms.</i>


## Les fonctions
### <font color="magenta">periode</font>
Cette fonction prend pour argument l'une des 26 lettres de l'alphabet. Chaque lettre correspond à une note de musique de 207.6Hz (sol#) à 880Hz (la) avec une note blanche (periode trop élevée pour être entendue). Elle retourne la periode du DMA correspondant à la note sachant qu'une periode de la sinusoïde du DAC se fait sur 360 échantillons.

### <font color="magenta">tourne</font>
Cette fonction prend en argument les 4 cases du Tetromino en cours de descente ainsi que les coordonnées du centre du Tetromino. Elle retourne les 4 nouvelles cases ainsi que les nouvelles coordonnées du Tetromino une fois qu'il est tourné de 90° dans le sens horaire. Cette fonction prend en compte et empêche l'Easy spin.

<center>
    <div style="width:50%">![Illustration de l'Easy Spin](img/easy_spin.gif "Illustration de l'Easy Spin")</div>
</center>

### <font color="magenta">Affiche_Pavee</font>
Cette fonction prend comme argument les 7 coordonnées de l'élément en cours de descente. Le centre du carré d'affichage ainsi que les 4 cases du Tetrominos plus la couleur du Tetromino. Elle ne renvoie rien mais affiche le pavé à l'endroit indiqué.

## Les fonctions d'interruptions
### <font color="grey">HAL_GPIO_EXTI_Callback</font>
Cette fonction est déclenchée par les boutons 1 et 2. Si le bouton 1 est enfoncé, elle charge <font color="green">endgame</font> à 1 et déclenche <font color="red">GameOver</font>. Si c'est le bouton 2 qui est enfoncé, elle change l'état de <font color="blue">music_on</font> et allume ou éteint le DMA.

### <font color="grey">HAL_UART_RxCpltCallback</font>
Cette fonction est déclenchée par la liaison série. Les commandes possibles sont les suivantes:

* 'z','q','s','d',' ' et 'r' chargent <font color="green">zqsd</font> ce qui permet à <font color="red">Pave</font> d'appliquer le déplacement au Tetromino descendant.
* '0','1,'2','3','4','5','6' et '7' chargent <font color="green">Lance_el_svt</font> et permettent de choisir l'élément suivant.
* 'p' charge <font color="green">endgame</font> ce qui permet de déclencher <font color="red">GameOver</font> (arret ou lancement du jeu).
* 'n' charge <font color="green">incr_score</font> de 5 ce qui permet d'augmenter artificiellement le score.

## Les Messageries
### <font color="green">zqsd</font>
Fait le lien entre le Joystick (via <font color="red">Joystick</font>) ou le terminal et <font color="red">Pave</font>.

### <font color="green">Lance_el_svt</font>
Fait le lien entre <font color="red">El_suivant</font> et <font color="red">Pave</font>.

### <font color="green">incr_score</font>
Fait le lien entre le terminal ou <font color="red">Effacelignes</font> et <font color="red">Affichage_temps</font>.

### <font color="green">endgame</font>
Fait le lien entre le bouton 1, le terminal ou <font color="red">Effacelignes</font> et <font color="red">GameOver</font>.

### <font color="green">joystick</font> (inutilisé)
Etait supposé faire le lient entre <font color="red">Joystick</font> et <font color="red">Pave</font> mais l'utilisation de <font color="green">zqsd</font> s'est révélée plus pertinente.

## Les Mutex
### <font color="orange">MutexEcran</font>
Permet de protéger les interactions avec l'écran.

### <font color="orange">Mutextableau</font>
Permet de protéger les interactions avec <font color="blue">Black_Board</font>.

### <font color="orange">positiontrucquibouge</font> (utilisé uniquement par <font color="red">Pave</font>, donc inutile)
A l'origine, Affiche_Pavee était une tache, j'avais donc créé ce Mutex pour protéger <font color="blue">positiontkb[7]</font> qui permettait la communication entre les deux taches mais lorsque Affiche_Pavee est devenu une fonction, je n'ai pas pris le soin de l'effacer.

### <font color="orange">Mutexelsvt</font>
Permet de protéger les interactions avec <font color="blue">elsvt</font>.

## Les variables globales
### <font color="blue">rxbuffer</font>
Permet de stocker momentanément les données de la liaison série avant de les envoyer par messagerie.

### <font color="blue">positiontkb</font> (aurait pu être une variable locale)
Stock les données du Tetromino descendant. Abscisse et Ordonnée du carrée d'affichage (en pixels) en position 0 et 1 respectivement. Numéro des carrés réelement utilisés en position 2 à 5. Couleur en position 6.

### <font color="blue">elsvt</font>
Forme et couleur de l'élément suivant, codé en nombre de 1 à 7 et 1 à 5 respectivement.

### <font color="blue">Black_Board</font>
Valeur (couleur) de chaque case du tableau. Initialisé à 0 pour "LIGHT_GREEN". Il n'est modifié que si les carrés sont définitivement positionés.

### <font color="blue">music_on</font>
Token permettant de savoir si la musique est en lecture ou non.

### <font color="blue">pointeur</font>
Pointeur permettant de savoir à quelle valeur de la sinusoïde (dans <font color="blue">sinus10bit</font>) le DAC se trouve.

### <font color="blue">longueur_partition_tetris</font>
Permet de relancer la partition une fois celle ci arrivée à terme.

### <font color="blue">tetris</font>
Partition de Tetris codée en lettre de l'alphabet.

### <font color="blue">sinus10bit</font>
Valeurs prises par le DAC pour générer une sinusoïde de 360 valeurs.
