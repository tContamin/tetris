# Bienvenu dans la partie musicale de ce projet

## Présentation DAC

<center>
    <div style="width:75%">![Schéma d'une conversion NA](img/CNAval.png "Illustration d'une conversion NA")</div>
</center>

Le DAC (ou convertisseur Numérique - Analogique) permet de transformer une suite de bits en un signal. Dans notre cas, un signal sonore. Le DAC de notre carte permet de travaller sur 8 ou 10 bits. Il ne peut pas travailler au delà pour des raisons techniques. En effet, pour travailler sur 12 bits, il faudrait des résistances très précises pour garantir les niveaux de chaque échelon.

Les schémas usuels des DAC sont les suivants

<center>
    <div style="width:75%">![Première version du DAC](img/CNA1.png "Schéma DAC 2")</div>
</center>

<center>
    <div style="width:75%">![Seconde version du DAC](img/CNA2.png "Schéma DAC 1")</div>
</center>

Dans le cas d'un signal sonore, la limite de 8 bits est largement suffisante puisque le haut-parleur lisse le signal et qu'on travaille à des fréquences suffisament basse. J'ai donc enregistré les valeurs d'une sinusoïde dans <font color="blue">sinus10bit[360]</font> et en les fournissant à mon DAC, je génère un signal sonore.

## Pourquoi un DMA

Le signal ainsi généré a l'aspect suivant:

<center>
    ![Sinus échantillonné](img/pseudosinus.png "Sinus échantilloné")
</center>

La fréquence du DMA est générée par interruptions sur timer 7. Une fois les données chargées, c'est une partie du processeur qui s'occupe de charger les valeurs de <font color="blue">sinus10bit[360]</font> dans le DAC et le processeur peut se concentrer sur l'OS temps réel. Si je n'utilisais pas un DMA, il aurait fallut interrompre toutes les opérations en cours toutes les 0.1ms pour changer la valeur du DAC, ça se serait trés vite révélé embetant pour un système temps réel.

Par ailleurs, sans changer le nombre d'échantillons, le fait de faire varier la fréquence DMA fait varier la Fréquence basse (ie la note). Ce faisant, je n'occupe pas la mémoire avec des dizaines de milliers de valeurs du sinus ou avec des calculs de sinus puisqu'un seul tableau contenant 360 valeurs du sinus me suffit.

## Problème rencontre

Malheureusement, malgrès la robustesse théorique de ce système, il semble y avoir un problème. A chaque changement de note, je ne touche pas au DMA puisque ce dernier lit en boucle les valeurs de <font color="blue">sinus10bit[360]</font> ce qui me convient tout à fait. Je ne fait que changer la periode du timer qui déclenche les interrputions 

        htim7.Init.Period = periode(tetris[pointeur]);
        #ou tetris[pointeur] cible l'une des notes du tetris et periode renvoie la periode correspondant

Le problème, c'est qu'au bout d'un certain nombre de changement de notes (aléatoire), on a

        HAL_DAC_GetState(&hdac) == HAL_DAC_STATE_TIMEOUT

Je ne sais pas à quoi correspondt cet état. Toujours est il que si je souhaite relancer le DMA, il faut utiliser

        HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)sinus10bit, 360, DAC_ALIGN_12B_R)

Cette commande fonctionne une fois sur deux. Parfois le DMA est relancé sans problème, parfois cela de marche pas et je tombe dans

        Error_Handler

Malgrès tout, avec un peu de chance, on peut entendre la musique de Tetris plusieurs fois avant d'entrer dans la boucle d'Erreur, je pense donc que cette partie du projet est une réussite même si ce n'est pas une réussite totale.

## Remarque

En passant trop de temps sur le problème décrit ci dessus, je n'ai pas pu générer la musique du GameOver même si la partition est relativement facile.

[Musique GameOver](https://www.youtube.com/watch?v=Iue_3T1Y2f8)

Oui, c'est du Mario, mais elle est bien mieux que celle de Tetris!!!

[Musique GameOver Tetris](https://www.youtube.com/watch?v=zLUEydTltHA)
