# Bienvenu dans la partie artistique de ce projet

<center>
    <div style="width:75%">![Partition de la musique tetris](img/partition.jpg "Partition Tetris")</div>
</center>

## Méthode de codage

Chacune des notes de la partition correspondt à une fréquence spécifique de sinusoïde. J'ai donc transformé la partition en un texte puis ce texte en fréquence.

## Temps faibles, temps forts

Chacune des notes de la partition est émise sur une certaine durée. Il fallait donc découper la partition en son plus petit interval de temps (en croches dans notre cas) puis remplir les espaces vides par des notes sourdes.

<center>
    <div style="width:75%">![Arbre représentant les notes de musique selon leur durée](img/notes.jpg "Notes de musique selon leur durée")</div>
</center>

## Liaison entre les notes

Si on émet ce genre de chose, on va entendre un claquement à chaque notes ce qui est très désagréable.

<center>
    <div style="width:75%">![Modulation de phase](img/modup.png "Transition catastrophique entre les notes")</div>
</center>

Le fait de changer la fréquence du DMA et non la séquence des notes permet de palier ce problème puisqu'on obtient alors un signal qui a cette forme ci:

<center>
    <div style="width:75%">![Modulation en fréquence](img/moduf.png "Bonne transition entre les notes")</div>
</center>

## Remarque

Ce qui est interessant avec ce procédé, c'est que peu importe la partition "simple", une fois convertie en charactères de l'alphabet, je peut faire jouer la partition à la carte.

## Cas des harmoniques

Pour générer mon signal, je me base sur les valeurs de <font color="blue">sinus10bit[360]</font>. Je ne peut donc pas générer deux notes en même temps (accords). Si j'avais modifié la fréquence basse (cf partie technique) et non la fréquence du DMA, en enregistrant les notes adaptées aux accords, j'aurais pu jouer des partitions plus complexes. On peut adapter cette méthode dans notre cas en réglant la fréquence correctement et en remplaçant <font color="blue">sinus10bit[360]</font> par les suites de valeurs de deux sinus mais on perdrait l'intert d'une utilisation du DMA.

Une autre solution serait d'utiliser les fonctions sinus du language C pour générer la partition que l'on veut. Il suffirait de travailler avec 2 bus, l'un de lecture et l'autre dont on changerait les valeurs. Le problème de cette méthode, c'est qu'elle nécessite un temps de calcul conséquent et serait moins performante.
